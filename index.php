<?php
include_once "vendor/autoload.php";

use Pondit\Calculator\AreaCalculator\Displayer;
use Pondit\Calculator\AreaCalculator\Rectangle;
use Pondit\Calculator\AreaCalculator\Square;
use Pondit\Calculator\AreaCalculator\Triangle;
use Pondit\Calculator\AreaCalculator\Circle;


$rectangle1=new Rectangle();
$rectangle1->width=5;
$rectangle1->length=3;
$displayer1=new Displayer();
$displayer1->displaypre($rectangle1->getRectangleArea());

$square1=new Square();
$square1->edge=5;
$displayer2=new Displayer();
$displayer2->displaypre($square1->getSquareArea());

$triangle1=new Triangle();
$triangle1->base=4;
$triangle1->height=5;
$displayer3=new Displayer();
$displayer3->displaypre($triangle1->getTriangleArea());

$circle1=new Circle();
$circle1->radius=5;
$displayer4=new Displayer();
$displayer4->displaypre($circle1->getCircleArea());