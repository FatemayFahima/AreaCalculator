<?php


namespace Pondit\Calculator\AreaCalculator;


class Displayer
{
    function displaysimple($story)
    {
        echo $story;
    }
    function displaypre($story)
    {
        echo "<pre>";
        echo $story;
        echo "</pre>";
        echo "<hr/>";
    }
    function displayH1($story)
    {
        echo "<h1>";
        echo $story;
        echo "</h1>";
    }
}