<?php

namespace Pondit\Calculator\AreaCalculator;


class Triangle
{
    public $base;
    public $height;
    public function getTriangleArea()
    {
        return 1/2*$this->base*$this->height;
    }
}