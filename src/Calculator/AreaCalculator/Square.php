<?php
/**
 * Created by PhpStorm.
 * User: Fahima
 * Date: 13-04-18
 * Time: 11.01
 */

namespace Pondit\Calculator\AreaCalculator;


class Square
{
    public $edge;
    public function getSquareArea()
    {
        return $this->edge*$this->edge;
    }
}