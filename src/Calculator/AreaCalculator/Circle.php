<?php
/**
 * Created by PhpStorm.
 * User: Fahima
 * Date: 13-04-18
 * Time: 12.26
 */

namespace Pondit\Calculator\AreaCalculator;


class Circle
{
    public $radius;
    public function getCircleArea()
    {
        return 3.1416*$this->radius*$this->radius;
    }
}